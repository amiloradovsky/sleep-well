let
  pkgs = import <nixpkgs> {};
  stdenv = pkgs.stdenv;
  with_dune = true;
in rec {
  sleep-well = stdenv.mkDerivation rec {
    name = "sleep-well";
    src = ./.;
    prePatch = with pkgs.ocamlPackages; ''
    substituteInPlace ./Makefile \
                      --replace /usr $out
    '';

    buildInputs = with pkgs; [
      ocamlPackages.ocaml
      ocamlPackages.findlib
      ocamlPackages.ocaml_lwt
      ocamlPackages.lwt_ppx
      ocamlPackages.tyxml
      stdenv
    ] ++ stdenv.lib.optional with_dune [
      ocamlPackages.ocaml-migrate-parsetree
      ocamlPackages.ppx_tools_versioned
      jbuilder
    ];
  };
}
