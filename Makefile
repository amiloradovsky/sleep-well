all: main tyxml_test

main: main.ml
	ocamlfind opt -linkpkg -package lwt_ppx,lwt.unix -o $@ $<

tyxml_test: tyxml_test.ml
	ocamlfind opt -linkpkg -package tyxml -o $@ $<

install:
	mkdir -p /usr/bin/
	install main /usr/bin/sleep-well
	install tyxml_test /usr/bin/

clean:
	rm -f main tyxml_test *.{o,cm{i,o,x}}
