let complement x = 1.0 -. x
let weighted a b x y = a *. x +. b *. y
let normed a x y = a *. x +. (complement a) *. y

let () =
  let ratio = ref (exp (-. 1.))
  and t0_ = ref None and t1_ = ref None in
  let specs = ["-ratio", Arg.Set_float ratio,
               "specifies the sleep ratio (0, 1)";
               "-have", Arg.Float (function x -> t0_ := Some x),
               "the time by which I got up (in hours)";
               "-should", Arg.Float (function x -> t1_ := Some x),
               "the time by which I should get up (in hours)"]
  and anon_args_warning _ =
    print_endline "Anonymous option ignored." in
  Arg.parse specs anon_args_warning
    "Arguments: [-ratio <float>] [-have <hours>] [-should <hours>]";
  (* TODO: How to properly integrate the options parsing with Lwt? *)

  let stdio_lock = Lwt_mutex.create () in
  Lwt_main.run @@
    let%lwt t0 =
      match !t0_ with Some x -> Lwt.return x | None ->
        let%lwt () = Lwt_mutex.lock stdio_lock in
        let%lwt () = Lwt_io.(write stdout) "When have you got up? " in
        let%lwt t0_data = Lwt_io.(read_line stdin) in
        let () = Lwt_mutex.unlock stdio_lock in
        Lwt.return (float_of_string t0_data)
    and t1 =
      match !t1_ with Some x -> Lwt.return x | None ->
        let%lwt () = Lwt_mutex.lock stdio_lock in
        let%lwt () = Lwt_io.(write stdout) "When should you get up? " in
        let%lwt t1_data = Lwt_io.(read_line stdin) in
        let () = Lwt_mutex.unlock stdio_lock in
        Lwt.return (float_of_string t1_data) in
    let%lwt () =
      Lwt_io.(write_line stdout) @@
        if t0 < t1 then String.concat " "
                          ["You should go to sleep at";
                           string_of_float (normed !ratio t0 t1)]
                   else "Seems like you've swapped the times!" in
    Lwt.return_unit
