open Tyxml

let to_ocaml = Html.(a ~a:[a_href "ocaml.org"] [pcdata "OCaml!"])
let mycontent =
  Html.(div ~a:[a_class ["content"]]
          [h1 [pcdata "A fabulous title"];
           p [pcdata "This is a fabulous content."];
           to_ocaml])
let mytitle = Html.(title (pcdata "A Fabulous Web Page"))
let mypage = Html.(html (head mytitle []) (body [mycontent]))
let () =
  let file = open_out "index.html" in
  let fmt = Format.formatter_of_out_channel file in
  Html.pp () fmt mypage;
  close_out file
